\context ChordNames
	\chords {
		\set chordChanges = ##t
		% intro
		c1 e1:m f1 g1

		% uh
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% si conocieras como te amo...
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% si cooonocieras...
		a1:m e1:m f2 g2
		c2 g2

		% uh
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% si conocieras como te busco...
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% si cooonocieras...
		a1:m e1:m f2 g2
		c2 g2

		% uh
		c1 e1:m f1 g1
		c1 e1:m f1 g1

		% si cooonocieras...
		a1:m e1:m f2 g2
		c1
	}
