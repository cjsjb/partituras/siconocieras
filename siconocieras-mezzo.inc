\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key c \major

		R1*4  |
%% 5
		e' 1  |
		d' 1  |
		c' 2 ~ c' 8 a ( c' d' ~  |
		d' 2. ) r4  |
		e' 1  |
%% 10
		d' 1  |
		c' 2 ~ c' 8 a ( c' d' ~  |
		d' 2. ) r16 e' e' f'  |
		g' 16 g' g' g' 8 g' 16 g' 8. g' 8 r16 r g' a' g'  |
		g' 16 g' g' g' 8 g' 16 g' 8. g' 8 r16 r8 g' 16 g'  |
%% 15
		g' 16 f' 8. f' 8 e' 16 e' 8. d' 8 c' d' ~  |
		d' 2 ~ d' 8 r r16 e' e' f'  |
		e' 16 e' e' e' 8 e' 16 e' 8. e' 8 e' 16 e' 8. e' 16  |
		e' 16 e' 8 -\staccato e' 16 e' e' e' 8. e' 8 r16 r8 e' 16 e'  |
		c' 16 c' 8 c' 16 c' 8 c' 16 c' 8. c' 8 a 16 c' 8.  |
%% 20
		d' 2 ~ d' 8 r16 d' e' 8. d' 16  |
		c' 8 c' 4. r8 r16 c' e' 8. c' 16  |
		b 8 b 4. r8 r16 b e' 8. b 16  |
		a 8 a 4 ~ a 16 a b b 8. b g 16  |
		g 2 r  |
%% 25
		e' 1  |
		d' 1  |
		c' 2 ~ c' 8 a ( c' d' ~  |
		d' 2. ) r4  |
		e' 1  |
%% 30
		d' 1  |
		c' 2 ~ c' 8 a ( c' d' ~  |
		d' 2. ) r16 e' e' f'  |
		g' 16 g' g' g' 8 g' 16 g' 8. g' 8 r16 r g' a' g'  |
		g' 16 g' g' g' 8 g' 16 g' 8. g' 8 r16 r8 g' 16 g'  |
%% 35
		g' 16 f' 8 f' 16 f' 8 e' 16 e' 8. d' 8 c' d' ~  |
		d' 2 ~ d' 8 r r16 e' e' f'  |
		e' 16 e' e' e' 8 e' 16 e' 8. e' 8 e' 16 e' 8. e' 16  |
		e' 16 e' 8 -\staccato e' 16 e' e' e' 8. e' 8 r16 r8 e' 16 e'  |
		c' 16 c' 8. c' 8 c' 16 c' 8. c' 8 a 16 c' 8.  |
%% 40
		d' 2 ~ d' 8 r16 d' e' 8. d' 16  |
		c' 8 c' 4. r8 r16 c' e' 8. c' 16  |
		b 8 b 4. r8 r16 b e' 8. b 16  |
		a 8 a 8. a 16 a a b b 8. b g 16  |
		g 2 r  |
%% 45
		e' 1  |
		d' 1  |
		c' 2 ~ c' 8 a ( c' d' ~  |
		d' 2. ) r4  |
		e' 1  |
%% 50
		d' 1  |
		c' 2 ~ c' 8 a ( c' d' ~  |
		d' 2 ~ d' 8 ) r16 d' e' 8. d' 16  |
		c' 8 c' 4. r8 r16 c' e' 8. c' 16  |
		b 8 b 4. r8 r16 b e' 8. b 16  |
%% 55
		a 8 a 4 a 16 a b b 8. b g 16  |
		g 2. r4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		% uhs
		Uh, uh, uh, __ uh. __
		Uh, uh, uh, __ uh. __

		%
		Si co -- no -- cie -- ras có -- mo te a -- mo,
		si co -- no -- cie -- ras có -- mo te a -- mo,
		de -- ja -- rí -- as de vi -- vir sin a -- mor. __

		%
		Si co -- no -- cie -- ras có -- mo te a -- mo,
		si co -- no -- cie -- ras có -- mo te a -- mo,
		de -- ja -- rí -- as de men -- di -- gar cual -- quier a -- mor. __

		%
		Si co -- no -- cie -- ras
		có -- mo te a -- mo,
		có -- mo te a -- mo, __
		se -- rí -- as más fe -- liz.

		% uhs
		Uh, uh, uh, __ uh. __
		Uh, uh, uh, __ uh. __

		%
		Si co -- no -- cie -- ras có -- mo te bus -- co,
		si co -- no -- cie -- ras có -- mo te bus -- co,
		de -- ja -- rí -- as que "te al" -- can -- za -- ra mi voz. __

		%
		Si co -- no -- cie -- ras có -- mo te bus -- co,
		si co -- no -- cie -- ras có -- mo te bus -- co,
		de -- ja -- rí -- as que "te ha" -- bla -- "ra al" co -- ra -- zón. __

		%
		Si co -- no -- cie -- ras
		có -- mo te bus -- co,
		có -- mo te bus -- co,
		es -- cu -- cha -- rí -- as más mi voz.

		% uhs
		Uh, uh, uh, __ uh. __
		Uh, uh, uh, __ uh. __

		% 
		Si co -- no -- cie -- ras
		có -- mo te sue -- ño,
		có -- mo te sue -- ño,
		pen -- sa -- rí -- as más en mí.
	}
>>
