\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key c \major

		R1*4  |
%% 5
		g 1  |
		g 1  |
		g 2 ~ g 8 c' ( b g ~  |
		g 2. ) r4  |
		c' 2.. d' 16 ( c'  |
%% 10
		b 8 a 16 g ~ g 2 ) r8 a 16 ( g  |
		a 1 )  |
		g 2. r16 e e f  |
		g 16 g g g 8 g 16 g 8. g 8 r16 r e e f  |
		g 16 g g g 8 g 16 g 8. g 8 r16 r8 g 16 g  |
%% 15
		g 16 f 8. f 8 e 16 e 8. d 8 c d ~  |
		d 2 ~ d 8 r r16 e e f  |
		g 16 g g g 8 g 16 g 8. g 8 g 16 c' 8. b 16  |
		a 16 g 8 -\staccato g 16 g g g 8. g 8 r16 r8 g 16 g  |
		g 16 f 8 f 16 f 8 e 16 e 8. d 8 d 16 c 8.  |
%% 20
		d 2 ~ d 8 r16 d e 8. d 16  |
		c 8 c 4. r8 r16 c e 8. c 16  |
		b, 8 b, 4. r8 r16 b, e 8. b, 16  |
		a, 8 a 4 ~ a 16 a g f 8. f e 16  |
		e 2 r  |
%% 25
		g 1  |
		g 1  |
		g 2 ~ g 8 c' ( b g ~  |
		g 2. ) r4  |
		c' 2.. d' 16 ( c'  |
%% 30
		b 8 a 16 g ~ g 2 ) r8 a 16 ( g  |
		a 1 )  |
		g 2. r16 e e f  |
		g 16 g g g 8 g 16 g 8. g 8 r16 r e e f  |
		g 16 g g g 8 g 16 g 8. g 8 r16 r8 g 16 g  |
%% 35
		g 16 f 8 f 16 f 8 e 16 e 8. d 8 c d ~  |
		d 2 ~ d 8 r r16 e e f  |
		g 16 g g g 8 g 16 g 8. g 8 g 16 c' 8. b 16  |
		a 16 g 8 -\staccato g 16 g g g 8. g 8 r16 r8 g 16 g  |
		g 16 f 8. f 8 e 16 e 8. d 8 d 16 c 8.  |
%% 40
		d 2 ~ d 8 r16 d e 8. d 16  |
		c 8 c 4. r8 r16 c e 8. c 16  |
		b, 8 b, 4. r8 r16 b, e 8. b, 16  |
		a, 8 a 8. a 16 a a g f 8. f e 16  |
		e 2 r  |
%% 45
		g 1  |
		g 1  |
		g 2 ~ g 8 c' ( b g ~  |
		g 2. ) r4  |
		c' 2.. d' 16 ( c'  |
%% 50
		b 8 a 16 g ~ g 2 ) r8 a 16 ( g  |
		a 1 )  |
		g 2 ~ g 8 r16 d e 8. d 16  |
		c 8 c 4. r8 r16 c e 8. c 16  |
		b, 8 b, 4. r8 r16 b, e 8. b, 16  |
%% 55
		a, 8 a 4 a 16 a g f 8. f e 16  |
		e 2. r4  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		% uhs
		Uh, uh, uh, __ uh. __
		Uh, uh, __ uh, __ uh.

		%
		Si co -- no -- cie -- ras có -- mo te a -- mo,
		si co -- no -- cie -- ras có -- mo te a -- mo,
		de -- ja -- rí -- as de vi -- vir sin a -- mor. __

		%
		Si co -- no -- cie -- ras có -- mo te a -- mo,
		si co -- no -- cie -- ras có -- mo te a -- mo,
		de -- ja -- rí -- as de men -- di -- gar cual -- quier a -- mor. __

		%
		Si co -- no -- cie -- ras
		có -- mo te a -- mo,
		có -- mo te a -- mo, __
		se -- rí -- as más fe -- liz.

		% uhs
		Uh, uh, uh, __ uh. __
		Uh, uh, __ uh, __ uh.

		%
		Si co -- no -- cie -- ras có -- mo te bus -- co,
		si co -- no -- cie -- ras có -- mo te bus -- co,
		de -- ja -- rí -- as que "te al" -- can -- za -- ra mi voz. __

		%
		Si co -- no -- cie -- ras có -- mo te bus -- co,
		si co -- no -- cie -- ras có -- mo te bus -- co,
		de -- ja -- rí -- as que "te ha" -- bla -- "ra al" co -- ra -- zón. __

		%
		Si co -- no -- cie -- ras
		có -- mo te bus -- co,
		có -- mo te bus -- co,
		es -- cu -- cha -- rí -- as más mi voz.

		% uhs
		Uh, uh, uh, __ uh. __
		Uh, uh, __ uh, __ uh.

		% 
		Si co -- no -- cie -- ras
		có -- mo te sue -- ño,
		có -- mo te sue -- ño,
		pen -- sa -- rí -- as más en mí.
	}
>>
